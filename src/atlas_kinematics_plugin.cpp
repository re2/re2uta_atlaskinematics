
#include <atlas_kinematics_plugin/atlas_kinematics_plugin.h>
#include <pluginlib/class_list_macros.h>

// register plugin as a KinematicsBase implementation
PLUGIN_DECLARE_CLASS(re2uta_atlasKinematics, AtlasKinematicsPlugin,
             re2uta_atlasKinematics::AtlasKinematicsPlugin,
             kinematics::KinematicsBase)

namespace re2uta_atlasKinematics {
 
AtlasKinematicsPlugin::AtlasKinematicsPlugin():active_(false){}

bool AtlasKinematicsPlugin::isActive()
{
  if (active_)
    return true;
  return false;
}

bool AtlasKinematicsPlugin::initialize(const std::string& group_name,
                       const std::string& base_name,
                       const std::string& tip_name,
                       const double& search_discretization)
{
  setValues(group_name, base_name, tip_name, search_discretization);

  // initialize stuff here like load the robot model

  return true;
}

bool AtlasKinematicsPlugin::getPositionIK(const geometry_msgs::Pose &ik_pose,
                      const std::vector<double> &ik_seed_state,
                      std::vector<double> &solution,
                      int &error_code)
{
  if (!active_) {
    ROS_ERROR("Atlas Kinematics not active");
    error_code = kinematics::NO_IK_SOLUTION;
    return false;
  }

  // looks like ik_pose is the desired pose of the endpoint
  // ik_seed_state seems to be the joint angles to start the ik algorithm 
  // with (i.e. current arm joint angles)
  // solution are the joint angles from the ik
  // error codes found are:
  // kinematics::NO_IK_SOLUTION
  // kinematics::SUCCESS

  return true;
}

bool AtlasKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose &ik_pose,
                         const std::vector<double> &ik_seed_state,
                         const double &timeout,
                         std::vector<double> &solution,
                         int &error_code)
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
    error_code = kinematics::INACTIVE;
    return false;
  }

  // ik_pose is the desired pose of the endpoint
  // ik_seed_state seems to be the joint angles to start the ik algorithm 
  // with (i.e. current arm joint angles)
  // timeout must be some maximum time to do the ik search
  // solution are the joint angles from the ik
  // error codes found are:
  // kinematics::NO_IK_SOLUTION
  // kinematics::SUCCESS


  return true;
}

bool AtlasKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose &ik_pose,
                         const std::vector<double> &ik_seed_state,
                         const double &timeout,
                         const unsigned int& redundancy,
                         const double &consistency_limit,
                         std::vector<double> &solution,
                         int &error_code)
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
    error_code = kinematics::INACTIVE;
    return false;
  }

  // ik_pose is the desired pose of the endpoint
  // ik_seed_state seems to be the joint angles to start the ik algorithm 
  // with (i.e. current arm joint angles)
  // timeout must be some maximum time to do the ik search
  // solution are the joint angles from the ik
  // error codes found are:
  // kinematics::NO_IK_SOLUTION
  // kinematics::SUCCESS

  return true;
}


bool AtlasKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose &ik_pose,
                         const std::vector<double> &ik_seed_state,
                         const double &timeout,
                         std::vector<double> &solution,
                         const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &desired_pose_callback,
                         const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &solution_callback,
                         int &error_code)
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
    error_code = kinematics::INACTIVE;
    return false;
  }

  return true;
}

bool AtlasKinematicsPlugin::searchPositionIK(const geometry_msgs::Pose &ik_pose,
                         const std::vector<double> &ik_seed_state,
                         const double &timeout,
                         const unsigned int& redundancy,
                         const double &consistency_limit,
                         std::vector<double> &solution,
                         const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &desired_pose_callback,
                         const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &solution_callback,
                         int &error_code)
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
    error_code = kinematics::INACTIVE;
    return false;
  }

  return true;
}


bool AtlasKinematicsPlugin::getPositionFK(const std::vector<std::string> &link_names,
                      const std::vector<double> &joint_angles,
                      std::vector<geometry_msgs::Pose> &poses)
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
    return false;
  }

  return true;
}


const std::vector<std::string>& AtlasKinematicsPlugin::getJointNames() const
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
  }

  const std::vector<std::string> toMakeCompilerHappy;

  return toMakeCompilerHappy;
}

const std::vector<std::string>& AtlasKinematicsPlugin::getLinkNames() const
{
  if (!active_) {
    ROS_ERROR("kinematics not active");
  }

  const std::vector<std::string> toMakeCompilerHappy;

  return toMakeCompilerHappy;
}

} // namespace
