
#ifndef ATLAS_KINEMATICS_PLUGIN_H
#define ATLAS_KINEMATICS_PLUGIN_H

#include <kinematics_base/kinematics_base.h>

namespace atlas_arm_kinematics
{
class AtlasKinematicsPlugin : public kinematics::KinematicsBase
{
 public:

  AtlasKinematicsPlugin();

  bool isActive();

  virtual bool getPositionIK(const geometry_msgs::Pose &ik_pose,
                 const std::vector<double> &ik_seed_state,
                 std::vector<double> &solution,
                 int &error_code);

  virtual bool searchPositionIK(const geometry_msgs::Pose &ik_pose,
                const std::vector<double> &ik_seed_state,
                const double &timeout,
                const unsigned int& redundancy,
                const double &consistency_limit,
                std::vector<double> &solution,
                int &error_code);

  virtual bool searchPositionIK(const geometry_msgs::Pose &ik_pose,
                const std::vector<double> &ik_seed_state,
                const double &timeout,
                std::vector<double> &solution,
                const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &desired_pose_callback,
                const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &solution_callback,
                int &error_code);

  virtual bool searchPositionIK(const geometry_msgs::Pose &ik_pose,
                const std::vector<double> &ik_seed_state,
                const double &timeout,
                const unsigned int& redundancy,
                const double &consistency_limit,
                std::vector<double> &solution,
                const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &desired_pose_callback,
                const boost::function<void(const geometry_msgs::Pose &ik_pose, const std::vector<double> &ik_solution, int &error_code)> &solution_callback,
                int &error_code);

  virtual book getPositionFK(const std::vector<std::string> &link_names,
                 const std::vector<double> &joint_angles,
                 std::vector<geometry_msgs::Pose> &poses);

  virtual bool initialize(const std::string& group_name,
              const std::string& base_name,
              const std::string& tip_name,
              const double& search_discretization);

  const std::vector<std::string>& getJointNames() const;

  const std::vector<std::string>& getLinkNames() const;

 protected:

  bool active_;
 };
}

#endif
